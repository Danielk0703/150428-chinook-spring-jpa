package at.spenger.jpa.test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import at.spenger.jpa.Config;
import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Playlist;
import at.spenger.jpa.service.ArtistService;
import at.spenger.jpa.service.PlayListService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
public class JUTests {
	@Autowired
	private PlayListService playListService;
	@Autowired
	private ArtistService artistService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		
	}
	
	@Test
	public void testArtist() {
		Artist a = artistService.findOne(1);
		assertThat(a.getName(), Matchers.is("AC/DC"));
	}

	@Test
	@Transactional
	public void testPlaylistTrack() {
		List<Playlist> l = playListService.findAll();
		assertThat(l.get(0).getTracks().get(0).getName(), Matchers.is("For Those About To Rock (We Salute You)"));
	}
	
	@Test
	public void testPlaylistTrackLength() {
		List<Playlist> l = playListService.findAll();
		assertThat(l.size(), Matchers.is(18));
	}
	
	@Test
	public void testArtistLength() {
		assertThat(artistService.length(), Matchers.is(275l));
	}
	
	@Test
	public void artistCount() {
		assertThat(artistService.length(), is(equalTo(275L)));
	}

	@Test
	public void artistLikeAc() {
		List<Artist> l = artistService.findByNameLike("Ac%");
		assertThat(l.size(), is(equalTo(7)));
	}
	
	@Test
	public void artistAcDc() {
		Artist a = artistService.findOne(1);
		assertThat(a.getName(), is(equalTo("AC/DC")));
	}
	
	

}

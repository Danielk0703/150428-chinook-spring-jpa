package at.spenger.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import at.spenger.jpa.model.Genre;

@Component("genreService")
@Transactional
public class GenreServiceImpl implements GenreService {
	private final GenreRepository genreRepository;
	
	@Autowired
	public GenreServiceImpl(GenreRepository genreRepository) {
		this.genreRepository = genreRepository;
	}
	
	@Override
	public List<Genre> findByNameLike(String name) {
		return genreRepository.findByNameLike(name);
	}

	@Override
	@Transactional(readOnly=true)
	public Genre findOne(int id) {
		return genreRepository.findOne(id);
	}

	@Override
	@Transactional(readOnly=true)
	public long length() {
		return genreRepository.count();
	}

	@Override
	public List<Genre> findAll() {
		return genreRepository.findAll();
	}

}

package at.spenger.jpa.service;

import java.util.List;

import at.spenger.jpa.model.Playlist;

public interface PlayListService {
	List<Playlist> findAll();
}

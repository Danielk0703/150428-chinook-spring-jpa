package at.spenger.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import at.spenger.jpa.model.Playlist;
@Component("playListService")
@Transactional
public class PlayListServiceImpl implements PlayListService {
	private PlayListRepository playListRepository;
	
	@Autowired
	public PlayListServiceImpl(PlayListRepository playListRepository) {
		this.playListRepository = playListRepository;
	}
	
	@Override
	public List<Playlist> findAll() {
		return playListRepository.findAll();
	}
}

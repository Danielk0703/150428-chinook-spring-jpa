package at.spenger.jpa.service;

import java.util.List;

import at.spenger.jpa.model.Genre;

public interface GenreService {
	List<Genre> findByNameLike(String name);
	Genre findOne(int id);
	long length();
	List<Genre> findAll();
}

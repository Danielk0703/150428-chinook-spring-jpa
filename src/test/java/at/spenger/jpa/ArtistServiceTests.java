package at.spenger.jpa;



import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.spenger.jpa.model.Artist;
import at.spenger.jpa.service.ArtistService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
public class ArtistServiceTests {
	@Autowired
	private ArtistService artistService;

	@Test
	public void artistCount() {
		assertThat(artistService.length(), is(equalTo(275L)));
	}

	@Test
	public void artistLikeAc() {
		List<Artist> l = artistService.findByNameLike("Ac%");
		assertThat(l.size(), is(equalTo(7)));
	}
	
	@Test
	public void artistAcDc() {
		Artist a = artistService.findOne(1);
		assertThat(a.getName(), is(equalTo("AC/DC")));
	}

}
